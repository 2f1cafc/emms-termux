(require 'emms-player-simple)

(defgroup emms-termux nil
  "Settings for the EMMS player which uses termux media player")

(defcustom emms-termux-command-name
  "termux-media-player"
  "Command that should be executed to play media within termux"
  :group 'emms-termux)

(defun emms-termux-do-command (cmd &rest args)
  (apply
   #'start-process "emms-termux" "emms-termux-buffer" emms-termux-command-name cmd args))

(defun emms-termux-start  (track)
  (message "emms termux start")
  (emms-termux-do-command "play" (emms-track-name track)))

(defun emms-termux-stop (track)
  (message "emms termux stop"))

(defun emms-termux-playablep (track)
  (message "track playable?: [%s]" (emms-track-name track))
  t)

(setq emms-termux-player
	  (emms-player #'emms-termux-start #'emms-termux-stop #'emms-termux-playablep))

(defun emms-termux-add-player (&optional append)
  (add-to-list 'emms-player-list emms-termux-player append))

(emms-termux-add-player)
; (setq emms-termux-command-name "echo")
